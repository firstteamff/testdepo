![image](https://i.hizliresim.com/Z931rG.jpg)

###KİŞİSEL BİLGİLER###
---
**İsim :** Çağatay TOPAÇ  
**Doğum Yeri :** Çorlu/TEKİRDAĞ  
**Doğum Tarihi :**  23.04.1997  
**Uyruğu:** TC  
**Medeni Durumu:** Bekar  
**Askerlik Durumu:** Tecilli  
**Ehliyet:** B2  
**Telefon:** 0534 293 13 06  
**E-Mail:** cagataytopac@outlook .com
###İŞ TECRÜBELERİ###
---
* *İstanbul Atatürk Havalimanı DHMİ, Stajyer*
* *ETOM Teknoloji Arge Edirne Teknopark,Part-time Bilgisayar Mühendisi*
###ÖĞRENİM DURUMU###
---
* *2015-2019 Trakya Üniversitesi Bilgisayar Mühendisliği Lisans*
* 2011-2015 MRU Anadolu Teknik Lisesi Çorlu/Tekirdağ
###YABANCI DİL###
---
* *ingilizce*
* *Fransızca*
###BİLGİSAYAR BECERİLERİ###
---
* *C*
* *C++*
* *C#*
* *VİSUAL BASİC*
* *HTML5* [CagatayTOPAC](CagatayTOPAC.16mb.com)
* *CSS3*
* *PHP*
* *DOCKER*
* *GIT*
###KURS VE SERTİFİKALAR###
---
* *SOFTTEC "Teoriden Pratiğe Yazılım Teknolojileri ve Süreçleri" Katılım Sertifikası*
*  *Dil Eğitimleri Atolyesi Fransızca A1 KAtılım Belgesi*
*  *Edirne Belediyesi Diksiyon Sertifikası*
*  *Milli Eğitim Bakanlığı Elektrik-Elektronik İşyeri Açma Belgesi* 
###REFERANSLAR###
---
* **Uğur Cem Yüksel** Tel: 0534 279 43 69 Bilgisayar Mühendisi
* **Mehmet Kesiktaş** Tel: 0532 175 08 02 Müdür